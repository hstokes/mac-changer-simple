#!/usr/bin/env bash

readonly PROJECT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

readonly ARG="${1}"

load_packages() {
  for package in "notification"; do

    # shellcheck source=/dev/null
    source "${PROJECT_PATH}/${package}.sh"
  done
}

macchanger_installed() {

  # TODO find right combination with -q option (true/false)
  [[ $(apt list --installed 2>/dev/null | grep -c "macchanger") -gt 0 ]]
}

should_reset_mac() {
  case ${ARG} in
  "--default")
    return 0
    ;;
  *)
    return 1
    ;;
  esac
}

# https://unix.stackexchange.com/questions/14961/how-to-find-out-which-interface-am-i-using-for-connecting-to-the-internet
get_network_interface() {
  # host we want to "reach"
  local -r HOST=google.com

  # get the ip of that host (works with dns and /etc/hosts. In case we get
  # multiple IP addresses, we just want one of them
  local -r HOST_IP=$(getent ahosts "${HOST}" | awk '{print $1; exit}')

  # only list the interface used to reach a specific host/IP. We only want the part
  # between dev and src (use grep for that)
  ip route get "${HOST_IP}" | grep -Po '(?<=(dev )).*(?= src| proto)'
}

reset_mac() {
  local -r INTERFACE=$(get_network_interface)

  # bring device offline
  sudo ifconfig "${INTERFACE}" down

  # restore default MAC address
  sudo macchanger --permanent "${INTERFACE}" >/dev/null 2>&1

  # bring device back online
  sudo ifconfig "${INTERFACE}" up

  sudo service network-manager restart

  notification::notify "Default MAC address: ${INTERFACE} - $(cat /sys/class/net/"${INTERFACE}"/address)"
}

main() {
  load_packages

  if ! macchanger_installed; then
    notification::notify "Macchanger isn't installed!"

    exit 1
  elif should_reset_mac; then
    reset_mac

    exit 0
  fi

  local -r INTERFACE=$(get_network_interface)

  # bring device offline
  sudo ifconfig "${INTERFACE}" down

  # change MAC address
  sudo macchanger -r "${INTERFACE}" >/dev/null 2>&1

  # bring device back online
  sudo ifconfig "${INTERFACE}" up

  # restart manager in order to have working connection, mac doesn't get reset, only after rebooting
  sudo service network-manager restart

  notification::notify "New MAC address: ${INTERFACE} - $(cat /sys/class/net/"${INTERFACE}"/address)"

  exit 0
}

main "${@}"

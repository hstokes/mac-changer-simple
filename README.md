<div align="center">

# Change MAC address everytime.

</div>

### Navigation

- [Installation](#installation)
- [Usage](#usage)
- [Use On Startup](#use-on-startup)

---
### Installation

- `sudo apt install macchanger`

---
### Usage

Run `./script.sh`

---
### Use On Startup

1. Open **rc.local** file in text editor, eg. `sudo subl /etc/rc.local` and paste content of the *script.sh*.
2. Make it executable `sudo chmod +x /etc/rc.local`
3. Get current MAC address with `ifconfig` and save it somewhere.
4. Restart machine and check if MAC address has changed.
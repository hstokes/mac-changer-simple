#!/usr/bin/env bash

#######################################
# Print output through printf and desktop notification.
# Arguments:
#   string/s to print
# Returns:
#   none
# Example:
#   notification::notify "Hello World!" "Another hello." "" "Bigger space."
#######################################
notification::notify() {

  local message=""

  for var in "${@}"; do

    message+="\n${var}"

    # skip empty new lines
    if [[ ${#var} -gt 0 ]]; then
      printf "%s\n" "[ ${var} ]"
    fi
  done

  # display notification
  notify-send "mac-changer-simple" "${message}"
}

#######################################
# Print error message.
# Arguments:
#   error to print
# Returns:
#   none
# Example:
#   error::print "Failed to connect."
#######################################
notification::error() {

  local message=""

  for var in "${@}"; do

    message+="\n${var}"

    # skip empty new lines
    if [[ ${#var} -gt 0 ]]; then
      printf "%s\n" "[ $(date '+%d/%m/%Y %H:%M:%S') ] ${var}" >&2
    fi
  done

  # display notification
  notify-send "mac-changer-simple" "${message}"
}
